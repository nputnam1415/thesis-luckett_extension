#install.packages("tidyr")
#install.packages("abind")
#install.packages("truncnorm") #We use the truncated normal distribution in initialize() below.
library(abind)
library(tidyr)
library(truncnorm)

#pi returns an array of the ratio pi/mu, alternative policy treatment probability over generative policy treatment probability.
pi =function(data, b0=-log(2/3), b1=0, b2=0, mu=mu){#Baseline values provide constant p(A=1)=0.6
  value=exp(-(b0 + b1*data[,"S1",] + b2*data[,"S2",]))/(1+exp(-(b0 + b1*data[,"S1",] + b2*data[,"S2",])))
  return(abs((1-data[,"A",])-value)/mu)
}

#reference generates a reference distribution. for use in the optimize() function
reference  <- function(N,mu){
  refd=array(dim = c(51,4,N))
  colnames(refd)=c("A","S1","S2","U") #A is treatment, 2 state variables S, Utility/Value U
  
  refd[1,,]=rbind(rbinom(n=N,size=1,mu),rtruncnorm(n=N,a=-2.5,b=2.5),rtruncnorm(n=N,a=-2.5,b=2.5),rep(NA, N)) #U at T=0 shouldn't matter as long as it's the same in every entry.
  
  for(t in 2:dim(refd)[1]){
    refd[t,1:3,]=rbind(rbinom(n=N,1,mu), #New A (doesn't affect this round, only impacts next round)
                       ((3/4)*(2*refd[t-1,"A",]-1)*refd[t-1,"S1",])+((1/4)*refd[t-1,"S1",]*refd[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4), #update S1
                       ((3/4)*(1-2*refd[t-1,"A",])*refd[t-1,"S2",])+((1/4)*refd[t-1,"S1",]*refd[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4)) #update S2  
    refd[t-1,4,]=2*refd[t,"S1",]+refd[t,"S2",]-(1/4)*(2*refd[t-1,"A",]-1) #update utility based on new S1, new S2, old A  
    
    
  }
  refd=refd[51,c("S1","S2"),]#this is the first observation after a burn-in of 50.
  return(refd)
}

#The reference function, but generated from a specific seed for reproducibility
reference_seed <- function(N,mu,seed){
  set.seed(seed = seed)
  refd=array(dim = c(51,4,N))
  colnames(refd)=c("A","S1","S2","U") #A is treatment, 2 state variables S, Utility/Value U
  
  refd[1,,]=rbind(rbinom(n=N,size=1,mu),rtruncnorm(n=N,a=-2.5,b=2.5),rtruncnorm(n=N,a=-2.5,b=2.5),rep(NA, N)) #U at T=0 shouldn't matter as long as it's the same in every entry.
  
  for(t in 2:dim(refd)[1]){
    refd[t,1:3,]=rbind(rbinom(n=N,1,mu), #New A (doesn't affect this round, only impacts next round)
                       ((3/4)*(2*refd[t-1,"A",]-1)*refd[t-1,"S1",])+((1/4)*refd[t-1,"S1",]*refd[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4), #update S1
                       ((3/4)*(1-2*refd[t-1,"A",])*refd[t-1,"S2",])+((1/4)*refd[t-1,"S1",]*refd[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4)) #update S2  
    refd[t-1,4,]=2*refd[t,"S1",]+refd[t,"S2",]-(1/4)*(2*refd[t-1,"A",]-1) #update utility based on new S1, new S2, old A  
    
    
  }
  refd=refd[51,c("S1","S2"),]#this is the first observation after a burn-in of 50.
  return(refd)
}

#initialize generates T time points each for N under the generative policy mu
#mu is currently a state-independent probability function: P(A=1)=mu.
#We could make mu a more flexible probability function, but it hasn't come up yet because the generative policy should remain simple.
initialize  <- function(T,N,mu){
  step1=array(dim = c(T+50,4,N))
  colnames(step1)=c("A","S1","S2","U") #A is treatment, 2 state variables S, Utility/Value U
  
  step1[1,,]=rbind(rbinom(n=N,size=1,mu),rtruncnorm(n=N,a=-2.5,b=2.5),rtruncnorm(n=N,a=-2.5,b=2.5),rep(NA, N)) #U at T=0 shouldn't matter as long as it's the same in every entry.
  
  for(t in 2:dim(step1)[1]){
    step1[t,1:3,]=rbind(rbinom(n=N,1,mu), #New A (doesn't affect this round, only impacts next round)
                        ((3/4)*(2*step1[t-1,"A",]-1)*step1[t-1,"S1",])+((1/4)*step1[t-1,"S1",]*step1[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4), #update S1
                        ((3/4)*(1-2*step1[t-1,"A",])*step1[t-1,"S2",])+((1/4)*step1[t-1,"S1",]*step1[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4)) #update S2  
    step1[t-1,4,]=2*step1[t,"S1",]+step1[t,"S2",]-(1/4)*(2*step1[t-1,"A",]-1) #update utility based on new S1, new S2, old A  
    
  }
  step1=step1[51:(T+50),,] #remove first 50 points for burn-in.
  return(step1)
}

#Same as initialize but with a fixed seed for reproducibility
initialize_seed <- function(T,N,mu,seed){
  set.seed(seed = seed)
  step1=array(dim = c(T+50,4,N))
  colnames(step1)=c("A","S1","S2","U") #A is treatment, 2 state variables S, Utility/Value U
  
  step1[1,,]=rbind(rbinom(n=N,size=1,mu),rtruncnorm(n=N,a=-2.5,b=2.5),rtruncnorm(n=N,a=-2.5,b=2.5),rep(NA, N)) #U at T=0 shouldn't matter as long as it's the same in every entry.
  
  for(t in 2:dim(step1)[1]){
    step1[t,1:3,]=rbind(rbinom(n=N,1,mu), #New A (doesn't affect this round, only impacts next round)
                        ((3/4)*(2*step1[t-1,"A",]-1)*step1[t-1,"S1",])+((1/4)*step1[t-1,"S1",]*step1[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4), #update S1
                        ((3/4)*(1-2*step1[t-1,"A",])*step1[t-1,"S2",])+((1/4)*step1[t-1,"S1",]*step1[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4)) #update S2  
    step1[t-1,4,]=2*step1[t,"S1",]+step1[t,"S2",]-(1/4)*(2*step1[t-1,"A",]-1) #update utility based on new S1, new S2, old A  
    
  }
  step1=step1[51:(T+50),,]
  return(step1)
}

#For a given dataset {A1,S1,S1,U1} and policy pi, evaluate calculates theta.
evaluate <- function(data, N, T, pi, mu, gamma, b0, b1, b2, lambda){
  #The one line of actual code here has a lot of different parts. Our goal is to solve for theta based on equation (4) in section two of the Luckett paper. To that end, we are calculating:
  #solve(A)%*%B  
  #Where A is a square 2x2 matrix and B is 2*1 vector.
  #Both are composed of the states S1&S2, utility U, and probability p of observed treatment a for a given n and t.
  
  #A11=sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T) 
  #A21=sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T)
  #A12=sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T)
  #A22=sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T)
  
  #B1=sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T)
  #B2=sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T)
  #A=(1/N)*matrix(nrow=2,ncol=2, data=c(sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S1",1]),"S1",])-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S1",1])-1,"S1",]),sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S2",1]),"S2",])-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S2",1])-1,"S2",]),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S1",1]),"S1",])-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S1",1])-1,"S1",]),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S2",1]),"S2",])-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S2",1])-1,"S2",])))
  #B=(1/N)*c(sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))
  
  #If you make changes to A or B, simply past them into this sample setup.
  #theta=solve((1/N)*matrix(nrow=2,ncol=2, data=c(,,,)))%*%t(t((1/N)*c(,)))
  #theta=solve((1/N)*matrix(nrow=2,ncol=2, data=c(sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T) ,sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))))%*%t(t((1/N)*c(sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))))
  
  #solve(A)%*%B
  
  p=pi(data = data[1:(length(data[,"A",1])-1),,], b0 = b0, b1=b1, b2=b2, mu=mu)
  theta=solve((1/N)*matrix(nrow=2,ncol=2, data=c(sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T) ,sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S1",1]),"S1",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S2",1]),"S2",], na.rm = T)-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))) + (lambda*N^(-1/2)*diag(2))
  )  %*%t(t((1/N)*c(sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))))
  
  #theta=solve((1/N)*matrix(nrow=2,ncol=2, data=c(sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S1",1]),"S1",])-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S1",1])-1,"S1",]),sum(gamma*p*data[1:length(data[,"S1",1])-1,"S1",]*data[2:length(data[,"S2",1]),"S2",])-sum(p*data[1:length(data[,"S1",1])-1,"S1",]*data[1:length(data[,"S2",1])-1,"S2",]),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S1",1]),"S1",])-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S1",1])-1,"S1",]),sum(gamma*p*data[1:length(data[,"S2",1])-1,"S2",]*data[2:length(data[,"S2",1]),"S2",])-sum(p*data[1:length(data[,"S2",1])-1,"S2",]*data[1:length(data[,"S2",1])-1,"S2",]))))%*%t(t((1/N)*c(sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S1",1])-1,"S1",], na.rm = T),sum(data[1:(length(data[,"U",1])-1),"U",]*p*data[1:length(data[,"S2",1])-1,"S2",], na.rm = T))))
  
  return(theta)
}


#based on theta and an empirical reference distribution (NOT the dataset created by reference(), just the initial observations of the datset), optimize calculates the value function
optimize <- function(theta, data){
  #The first step is to estimate the value function for the policy based on the theta from step 2 and the empirical distribution of starting states.
  valuei <- function(x,theta=theta){ #For any given vector c("S1", "S2"), calculates value
    t(x)%*%theta
  }
  vlist=matrix(nrow=T,ncol=N,NA)
  for(i in 1:T){
    values=apply(X = data[i,c("S1","S2"),], MARGIN = 2, FUN = function(x) valuei(x, theta=theta)) #We only use initial conditions, not subsequent evolved states, to estimate the value.
    vlist[i,]=values}
  Vhat=mean(vlist) #Under the empirical distribution, all of these initial states are equally likely to occur and no other states are possible. So, the integral wrt this empirical distribution is just an unweighted average
  return(Vhat) #TEMP STEP: This is not the output we care about, I just haven't figures out how to optimize this yet.
}


#This is the same function as optimize(), but optimize_refd uses the 10,000 person reference distribution generated by reference() instead of the empirical distribution.
optimize_refd <- function(theta){
  #The first step is to estimate the value function for the policy based on the theta from step 2 and the empirical distribution of starting states.
  valuei <- function(theta=theta){ #For any given vector c("S1", "S2"), calculates value
    t(refd10000)%*%theta
  }
  values=valuei(theta=theta) #We only use initial conditions, not subsequent evolved states, to estimate the value.
  Vhat=mean(values) #Under the empirical distribution, all of these initial states are equally likely to occur and no other states are possible. So, the integral wrt this empirical distribution is just an unweighted average
  return(Vhat) #TEMP STEP: This is not the output we care about, I just haven't figures out how to optimize this yet.
}

#This is just a little trick we have to do to make optimizations only optimize across beta parameters.
#You'll see below how this function is used.
outerf <- function(data, N, T, mu, gamma, lambda){
  
  innerf <-function(beta){
    theta1=evaluate(data=data, N=N, T=T, pi=pi, mu=mu, gamma=gamma, b0=beta[1], b1=beta[2], b2=beta[3], lambda=lambda)
    vhat=optimize(theta1, data=data)
    return(-(vhat-sum(beta^2)))
  }
  return(innerf)
}

#Same as outerf(), but using the reference distribution functions.
outerf_refd <- function(data, N, T, mu, gamma, lambda){
  
  innerf <-function(beta){
    theta1=evaluate(data=data, N=N, T=T, pi=pi, mu=mu, gamma=gamma, b0=beta[1], b1=beta[2], b2=beta[3], lambda=lambda)
    vhat=optimize_refd(theta1)
    return(-(vhat-sum(beta^2)))
  }
  return(innerf)
}


#below is a example of how these functions will be used in output.R
#This functions should not be used except in benchmarking. Construct output data as needed from the other functions, or using this as a base.
benchmark <- function(N=N., T=T., mu=mu., gamma=gamma., lambda=lambda., b0=b0., b1=b1., b2=b2.){
  
  
  step1=initialize(T=T, N=N, mu=mu)
  interf = outerf_refd(data=step1, T=T, N=N, mu=mu, gamma=gamma, lambda=lambda)
  
  startp=optim(par = c(b0,b1,b2), interf, method="SANN", control=list(maxit=1000, trace=TRUE))
  
  op=optim(par = c(startp$par[1],startp$par[2],startp$par[3]), interf, method="BFGS", control=list(trace=TRUE))
  
  theta1=evaluate(data=step1, T=T, N=N,pi=pi, mu=mu, gamma=gamma, b0=op$par[1], b1=op$par[2], b2=op$par[3], lambda=lambda)
  vhat=optimize_refd(theta=theta1)
  a=c(op$par,vhat)
  return(a)
}


#After we've determined the optimal policy's parameters, the next step is to generate a new population using that policy!

#pi_post is distinct from pi because instead of outputing a matrix of everyone's ratios pi/mu, it just outputs the probability of treatment under pi.
#it calculates everyone's probabilities at a particular time point because that is the format that best suits initialize_post
pi_post=function(data, t, b0=-log(2/3), b1=0, b2=0){#Baseline values provide constant p(A=1)=0.6
  value=1/(1+exp(-(b0 + b1*data[t,"S1",] + b2*data[t,"S2",])))
  return(value)
}

#Initialize_post is the same as initialize, but instead state-independent treatment probability mu it takes the three optimal policy parameters for pi_post.
#T=100 and N=100 are default parameters because that's the size of Luckett's secondaray simulation samples.
initialize_post <- function(T=100,N=100, b0, b1, b2){
  step2=array(dim = c(T+50,4,N))
  colnames(step2)=c("A","S1","S2","U") #A is treatment, 2 state variables S, Utility/Value U
  step2[1,2:4,]=rbind(rtruncnorm(n=N,a=-2.5,b=2.5),rtruncnorm(n=N,a=-2.5,b=2.5),rep(NA, N)) #U at T=0 shouldn't matter as long as it's the same in every entry.
  step2[1,1,]=rbinom(n=N,size=1,pi_post(data=step2, t=1, b0=b0, b1=b1, b2=b2))
  for(t in 2:dim(step2)[1]){
    step2[t,2:3,]=rbind(((3/4)*(2*step2[t-1,"A",]-1)*step2[t-1,"S1",])+((1/4)*step2[t-1,"S1",]*step2[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4), #update S1
                        ((3/4)*(1-2*step2[t-1,"A",])*step2[t-1,"S2",])+((1/4)*step2[t-1,"S1",]*step2[t-1,"S2",])+rnorm(n=N,mean=0,sd=1/4)) #update S2  
    step2[t,1,]=rbind(rbinom(n=N,1,pi_post(data=step2, t=t, b0=b0, b1=b1, b2=b2))) #New A (doesn't affect this round, only impacts next round)
    
    step2[t-1,4,]=2*step2[t,"S1",]+step2[t,"S2",]-(1/4)*(2*step2[t-1,"A",]-1) #update utility based on new S1, new S2, old A  
    
  }
  step2=step2[51:(T+50),,]
  return(step2)
}

#We already found the optimal policy, so theta isn't necessary anymore. evaluate_post calculates the mean utility of the population.
#Note that this is the mean utility across all time points t, not just final utility. Whether that agrees with Luckett's method is up for debate.
evaluate_post <- function(N,T,data){
  m=mean(data[,"U",], na.rm = T)
  s=sd(data[,"U",], na.rm=T)
  paste("N=",N,", T=",T, ", U=", round(m,digits = 3)," (", round(s,digits=4), ")", sep="")
}




################################################
#IN PROGRESS: A MORE FLEXIBLE EVALUATE FUNCTION#
################################################
#The current Evaluate function only works for Phi={S1,S2}
#We want a function that can take any proposed Phi and calculate theta.
#The current roadblock to this function being usable is that in the Phi={S1,S2} case, it's answer does not agree with the current evaluate() function, and I don't know why.

#npar is the number of parameters in phi and theta, and phi is the T*npar*N array featuring all the data that goes into phi.
#examples of phi:
#linear (Phi={S1,S2}):                        phi=data[,c("S1","S2"),]
#quadratic (Phi={S1,S2,S1^2,S2^2}):           phi=abind(data[,c("S1","S2"),], data[,"S1",]*data[,"S1",], data[,"S2",]*data[,"S2",],along = 2)
#Interaction (Phi={S1,S2,S1^2,S2^2,S1*S2}):   phi=abind(data[,c("S1","S2"),], data[,"S1",]*data[,"S1",], data[,"S2",]*data[,"S2",], data[,"S1[",]*data[,"S2",],along = 2)
evaluate_flex <- function(data, N, T, pi, mu, gamma, b0, b1, b2, lambda, npar, phi){
  phi=phi
  philagmat=t(apply(X=phi[1:dim(phi)[1]-1,,], MARGIN =2, FUN = c)) #phi vector for all indices t. row index is t, column index is phi variables, all n's are stacked on top of each other.
  phileapmat=t(apply(X=phi[2:dim(phi)[1],,], MARGIN =2, FUN = c)) #phi vector for all indices t+1. row index is t, column index is phi variables, all n's are stacked on top of each other.
  longp=c(pi(data = data[1:(length(data[,"A",1])-1),,], b0 = b0, b1=b1, b2=b2, mu=mu)) #all p measures, as a vector (n's stacked atop each other.)
  Pphilagmat=t(longp*(t(philagmat)))#same as philagmat, but each variable has been multiplied by the appropriate ratio pi/mu
  #Pphilagmat=t(apply(X=philagmat, MARGIN=1, FUN=function(x) longp*x))#same as philagmat, but each variable has been multiplied by the appropriate ratio pi/mu
  longU=c(data[1:length(data[,"U",1])-1,"U",])
  UPphilagmat=t(longU*(t(Pphilagmat)))#same function used to multiply pi/mu into the matrix now multiplies in utility U.
  #UPphilagmat=t(apply(X=Pphilagmat, MARGIN=1, FUN=function(x) longU*x))#same function used to multiply pi/mu into the matrix now multiplies in utility U.
  
  theta=(solve((1/N)*(gamma*Pphilagmat%*%t(phileapmat)-Pphilagmat%*%t(philagmat)))# + (lambda*N^(-1/2)*diag(npar))
  )%*%((1/N)*apply(X=UPphilagmat, MARGIN=1, FUN=sum))
  return(theta)
}
